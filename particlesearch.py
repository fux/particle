# SPDX-FileCopyrightText: 2021 Mario Fux <mario@unormal.org>
#￼ SPDX-License-Identifier: GPL-2.0-or-later

from datetime import date
import json
import mysql.connector
import requests
import sys
import time

from PySide2.QtCore import Qt, Slot
from PySide2.QtGui import QPainter
from PySide2.QtWidgets import (QAction, QApplication, QComboBox, QHeaderView, QHBoxLayout, QLabel, QLineEdit,
                               QMainWindow, QPushButton, QTableWidget, QTableWidgetItem, QSizePolicy, 
                               QVBoxLayout, QWidget)
from PySide2.QtCharts import QtCharts
from PySide2.QtWidgets import QApplication, QLabel
from PySide2.QtWebEngineWidgets import QWebEngineView, QWebEnginePage


class Widget(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.items = 0
        
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="dbadmin",
            password="db123",
            database="bfodb"
        )

        self.mycursor = self.mydb.cursor()
        self.currentDbResult = ""
        
        # TODO: Filter table on the right side

        # TODO: Add keyboard shortcuts for the two search lines
        # TODO: Add number of search results
        # Left side of the application
        self.titleSearchLabel = QLabel("Search title:")
        self.titleSearchLine = QLineEdit()
        self.titleSearchNumber = QLabel("#")
        self.titleSearchLayout = QHBoxLayout()
        self.titleSearchLayout.addWidget(self.titleSearchLabel)
        self.titleSearchLayout.addWidget(self.titleSearchLine)
        self.titleSearchLayout.addWidget(self.titleSearchNumber)
        
        self.textSearchLabel = QLabel("Search text:")
        self.textSearchLine = QLineEdit()
        self.textSearchLayout = QHBoxLayout()
        self.textSearchLayout.addWidget(self.textSearchLabel)
        self.textSearchLayout.addWidget(self.textSearchLine)

        self.table = QTableWidget()
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(["URL", "Title"])
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        
        self.left = QVBoxLayout()
        self.left.setMargin(10)
        self.left.addLayout(self.titleSearchLayout)
        self.left.addLayout(self.textSearchLayout)
        self.left.addWidget(self.table)

        # Right
        self.titleLabel = QLabel("Title:")
        self.webView = QWebEngineView()
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(10)
        sizePolicy.setVerticalStretch(10)
        self.webView.setSizePolicy(sizePolicy)
        
        self.quit = QPushButton("Quit")
        self.plot = QPushButton("Plot")

        self.right = QVBoxLayout()
        self.right.setMargin(10)
        self.right.addWidget(self.titleLabel)
        self.right.addWidget(self.webView)
        # TODO: Add a correct way to have a big webview on the right side
        self.right.addWidget(QLabel("StudidandCheapWorkaroundForSomethingIDon'tKnowYetButWillKnowSoonLikeTheWidthOfTheRightSide"))

        # QWidget Layout
        self.layout = QHBoxLayout()

        #self.tableView.setSizePolicy(size)
        self.layout.addLayout(self.left)
        self.layout.addLayout(self.right)

        # Set the layout to the QWidget
        self.setLayout(self.layout)

        # Signals and Slots
        self.titleSearchLine.returnPressed.connect(self.fillTable)
        self.table.cellClicked.connect(self.fillWebView)

    @Slot()
    def quit_application(self):
        QApplication.quit()

       
    @Slot()
    def fillTable(self):
        
        # FIXME: What's wrong with searching "BUND"
        # => Nothing, it searches case-insensitive but my replace methode is case-sensitive thus finds "bund", "Bund", "BUND", etc. but just marks "BUND"
        # FIXME: What's wrong with searching "IPCC" - Finds something but doesn't mark them
        
        # Clear the table widget
        self.items = 0
        self.table.clearContents()
        self.table.setRowCount(0)
        
        #print("Title: " + self.titleSearchLine.text())
        #print("Text : " + self.textSearchLine.text())
        
        searchTerm = self.titleSearchLine.text()

        sql = "SELECT DISTINCT url, title, text, date FROM articles WHERE text LIKE '%" + searchTerm + "%'"
        self.mycursor.execute(sql)

        self.currentDbResult = self.mycursor.fetchall()
        
        self.titleSearchNumber.setText("#" + str(len(self.currentDbResult)))
        
        for result in self.currentDbResult:
            urlItem = QTableWidgetItem(result[0])
            titleItem = QTableWidgetItem(result[1])
            self.table.insertRow(self.items)
            self.table.setItem(self.items, 0, urlItem)
            self.table.setItem(self.items, 1, titleItem)
            self.items += 1

        # FIXME: "der Ausstieg" für zu einem IndexError: list index out of range
        # Problem seems to be when there are HTML tags between these words then it doesn't find anything and does the self.currentDbResult[0][2] is empty or out of range
        # To hightlight the search term we just enclose the searchTerm with <mark> tags in the HTML
        text = self.currentDbResult[0][2]
        
        text2 = text.replace(searchTerm, "<mark>" + searchTerm + "</mark>")
        
        #debugFile = open(searchTerm + ".html", "w")
        #debugFile.write(text2)
        #debugFile.close()
        
        self.webView.setHtml(text2)
        self.titleLabel.setText(self.currentDbResult[0][1].replace("\n", "") + " - " + str(self.currentDbResult[0][3]))
        
        #self.webView.findText("titleSearchLine.text()")
        
    @Slot()
    def fillWebView(self, row, column):
        #print("Row, column: " + str(row) + ", " + str(column) + " - " + self.table.item(row, column).text() + " - " + self.currentDbResult[row][0])
        self.titleLabel.setText(self.currentDbResult[row][1].replace("\n", "") + " - " + str(self.currentDbResult[row][3]))
        
        searchTerm = self.titleSearchLine.text()
        text = self.currentDbResult[row][2]
        text2 = text.replace(searchTerm, "<mark>" + searchTerm + "</mark>")
        self.webView.setHtml(text2)      

class MainWindow(QMainWindow):
    def __init__(self, widget):
        QMainWindow.__init__(self)
        self.setWindowTitle("Particles - Article lookup application")

        # Set up the menu
        self.menu = self.menuBar()
        self.fileMenu = self.menu.addMenu("File")

        exitAction = QAction("Exit", self)
        exitAction.setShortcut("Ctrl+Q")
        exitAction.triggered.connect(self.exit_app)

        self.fileMenu.addAction(exitAction)
        self.setCentralWidget(widget)

    @Slot()
    def exit_app(self, checked):
        QApplication.quit()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = Widget()
    # QMainWindow using QWidget as central widget
    window = MainWindow(widget)
    window.resize(1024, 768)
    window.show()

    sys.exit(app.exec_())
