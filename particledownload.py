# SPDX-FileCopyrightText: 2021 Mario Fux <mario@unormal.org>
#￼ SPDX-License-Identifier: GPL-2.0-or-later

from datetime import date
import cookiejar
import json
import mysql.connector
import os
import re
import requests
from requests.exceptions import ConnectionError
#from readability import Document
from preadability.readability import Document

mydb = mysql.connector.connect(
  host="localhost",
  user="dbadmin",
  password="db123",
  database="bfodb"
)

mycursor = mydb.cursor()

dateQuery = "SELECT MAX(date) FROM `articles`"

mycursor.execute(dateQuery)
result = mycursor.fetchall()

for i in result:
    myDate = i[0]

dateString = myDate.strftime("%Y%m%d"))


articleFile = "/home/fmario/Wolcha/ReadLaterNew2.txt"
# DEBUG: test file
#articleFile = "/home/fmario/Wolcha/ReadLaterNew-Test.txt"
golemCookieFile = "post-cookies-golem-de.txt"


a = 1

af = open(articleFile, "r")

currentDate = date.today()

url = "https://bshah.in/plasma-librem5.mp4"

# TODO: Get newest date from database and then as soon as you reach that date in the file leave the for loop


for line in af:
    
    #print("|" + line + "|")
    print(str(a))
    rating = 0

    
    #DEBUG.... Add "DEBUG" somewhere in the articles list file and stop there
    if line.startswith("DEBUG"):
        print("Howdy, I'm gone")
        break;
    
    # Date parsing
    # Check what kind of line it is
    if line == "\n": # if it's an empty line
        print("Found empty line...")
        continue
    elif line.startswith("2"): # if the line starts with a '2' it's a date line
        print("Found date...")
        print(line.replace("\n",""))
        # Construct the date
        year = line[0:4]
        month = line[4:6]
        day = line[6:8]
        #print("Year: " + year + day + month)
        currentDate = year + "-" + month + "-" + day
        print("Current date: " + currentDate)
        
        if currentDate == dateString:
            print("All new URLs read.")
            break
        
    elif line.startswith("http"): # if it starts with 'http' it's a line containing a link
        print("Found URL: " + line.replace("\n",""))
        
        lineList = line.split(" ", 1)
        # Check if the line contains a single URL or a URL and a title (plus optional rating at the end)
        if len(lineList) == 1:
            print("Just a URL...")
            url = line
            
            titleFound = False
            title = ""
        elif len(lineList) == 2:
            
            # Split URL from title (and rating ****) and choose title instead of doc.title then for INSERT below   
            titleFound = True
            url = lineList[0]
            title = lineList[1]
            
            if title.endswith(" *\n"):
                print("Found rating 1 ********************************************")
                title = title[:-2].strip()
                rating = 1
            if title.endswith(" **\n"):
                print("Found rating 2 ********************************************")
                title = title[:-3].strip()
                rating = 2
            if title.endswith(" ***\n"):
                print("Found rating 3 ********************************************")
                title = title[:-4].strip()
                rating = 3
            if title.endswith(" ****\n"):
                print("Found rating 4 ********************************************")
                title = title[:-5].strip()
                rating = 4
            if title.endswith(" *****\n"):
                print("Found rating 5 ********************************************")
                title = title[:-6].strip()
                rating = 5
        else:
            print("Some problem while splitting...")


        # Check for not working hosts like https://aiix.tk/dev/plasma-bigscreen-updated-beta-2-image-release-for-raspberry-pi-4/
        # Solution: https://stackoverflow.com/questions/48665846/check-if-a-website-exists-with-requests-isnt-working

        # Download the website from the url
        # TODO: handle different pages differently (like youtube, golem.de (video, multi-page), vimeo, etc.
        # TODO: Check for website with multi page articles
        
        # https://stackoverflow.com/questions/14742899/using-cookies-txt-file-with-python-requests
        
             
        # TODO: Extract this to a function
        # Workaround for golem.de which uses cookies
        
        if "youtube.com" in url.lower():
            # TODO: Handle youtube URLs
            print("Youtube URLs are not yet handled...")
            continue
        elif url.endswith(".mp4"):
            # TODO: Handle mp4 files and Co
            # Error normally: preadability.readability.readability.Unparseable: Document is empty
            print("mp4-files are not yet handled...")
            continue
        elif url.startswith("https://www.electronicsweekly.com/news/products/raspberry-pi-development/raspberry-pi-delivers-great-multi-room-audio-2015-09/"):
            print("This is an exception... ;-)")
            continue
        elif url.startswith("https://www.tesla.com/de_CH/blog/solar-roof?redirect=no"):
            print("This is an exception... ;-)")
            continue
        elif "golem.de" in url.lower():
            htmlFile = "output.html"
            
            wgetCommand = "wget --load-cookies=" + golemCookieFile + " " + url.replace("\n","") + " -O " + htmlFile
            print("wgetCommand: " + wgetCommand)
            os.system(wgetCommand)
            
            # Check if the output of wget is an empty file like for pages which doesn't exist anymore
            if os.stat(htmlFile).st_size == 0:
                print(url.replace("\n","") + " does't exist anymore: 404")
                continue;
            
            utfHtmlFile = "output2.html"
            os.system("iconv -t UTF-8 " + htmlFile + " -o " + utfHtmlFile)
            
            h = open(utfHtmlFile, "r")
            htmlText = h.read()
            h.close()
            
            os.system("rm " + htmlFile)
            os.system("rm " + utfHtmlFile)
            doc = Document(htmlText)
        else:
            try:
                response = requests.get(url)
                doc = Document(response.text)
            except ConnectionError:
                text = "<h1>Page not available anymore.</h1>"
  
        text = doc.summary()

        if not titleFound:
            title = doc.title()

        #fileName = "File-" + doc.title() + ".html"
        #f = open(fileName, "w")
        #f.write(doc.summary())
        #f.close()

        #FIXME: Error 
        #10966
#Found URL: https://bshah.in/plasma-librem5.mp4 plasma-librem5.mp4
#error getting summary: 
#Traceback (most recent call last):
  #File "/home/fmario/devel/articleDownload/preadability/readability/readability.py", line 185, in summary
    #self._html(True)
  #File "/home/fmario/devel/articleDownload/preadability/readability/readability.py", line 128, in _html
    #self.html = self._parse(self.input)
  #File "/home/fmario/devel/articleDownload/preadability/readability/readability.py", line 137, in _parse
    #doc, self.encoding = build_doc(input)
  #File "/home/fmario/devel/articleDownload/preadability/readability/htmls.py", line 21, in build_doc
    #doc = lxml.html.document_fromstring(decoded_page.encode('utf-8', 'replace'), parser=utf8_parser)
  #File "/usr/lib/python3/dist-packages/lxml/html/__init__.py", line 764, in document_fromstring
    #"Document is empty")
#lxml.etree.ParserError: Document is empty


        print("URL: " + url.replace("\n", "") + "</END>")
        print("Title: " + title.replace("\n", "") + "</END>")
        print("Rating: " + str(rating) + "</END>")
        print("Date: " + str(currentDate) + "</END>")
        #print("Text: " + text + "</END>")

        # TODO: Check for the size of doc.summary() (was too long for TEXT type in mysql e.g. for URL https://kdenlive.org/en/2020/12/kdenlive-20-12-is-out/)

        # sql = "INSERT INTO articles (url, title, rating, text, date) VALUES (\"URL\", \"TITLE\", 2, \"text\", \"2021-12-12\")"
        sql = "INSERT INTO articles (url, title, rating, text, date) VALUES(%s, %s, %s, %s, %s)"
        val = (url, title, rating, text, currentDate)
        mycursor.execute(sql, val)
        
        mydb.commit()
        print(mycursor.rowcount, "record inserted.")

        
        url = ""
        title = ""
        #TODO: Special handling for youtube URLs and other video URLS
        if "youtube" in line.lower():
            print("YT")
    else:
        print("Found some weird stuff...")
        
    print()

    a = a + 1

af.close()


#articleUrls = ["https://www.heise.de/tp/features/Luft-nach-oben-bei-der-Windkraft-5037082.html", "https://www.tagesschau.de/wirtschaft/finanzen/gamestop-short-squeeze-robinhood-reddit-trader-101.html", "https://www.golem.de/news/mozilla-google-und-microsoft-retten-mdn-web-doku-2101-153668.html", "https://www.golem.de/news/eu-kommission-tesla-kann-von-eu-batteriefoerderung-profitieren-2101-153693.html", "https://www.golem.de/news/enterprise-linux-red-hat-hatte-kein-interesse-mehr-an-centos-2101-153687.html", "https://www.golem.de/news/autopilot-waymo-chef-glaubt-nicht-an-autonom-fahrende-teslas-2101-153689.html", "https://www.golem.de/news/europa-huawei-baut-mobilfunkausruestung-bald-in-frankreich-2101-153682.html", "https://www.golem.de/news/semi-us-chipindustrie-will-lockerung-des-embargos-gegen-huawei-2101-153697.html", "https://de.wikipedia.org/wiki/Common_Vulnerabilities_and_Exposures", "https://de.wikipedia.org/wiki/CVSS"]

#print(articleUrls)
#i = 1

#for url in articleUrls:
    #print(url)
    #response = requests.get(url)
    #doc = Document(response.text)
    #today = date.today()

    #sql = "INSERT INTO articles (url, title, text, date) VALUES(%s, %s, %s, %s)"
    #val = (url, doc.title(), doc.summary(), today)
    #mycursor.execute(sql, val)
    
    #mydb.commit()
    #print(mycursor.rowcount, "record inserted.")
    
    #fileName = "File-" + str(i) + ".html"
    #f = open(fileName, "w")
    #f.write(doc.summary())
    #f.close()
    #i = i + 1


